<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Pepe_Davalos_V3
 */

?>

	<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="<?php echo get_template_directory_uri()?>/js/jquery-sectionsnap.js" type="text/javascript"></script>
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

	<script type="text/javascript">
		


		$(document).ready(function($) {

			AOS.init();

			var elementPosition = $('#box-init').offset();
			var footerElement = $('#contact').offset();
			

			$(window).scroll(function(){
				positionMenu();
				showMenuDesktop();
			});



			function showMenuDesktop(){
				heightMenuLang = $('.menu-lang').offset().top - $(window).scrollTop();
				if( $( window ).width() > 992){
					$('nav').removeClass('bg-cover-pepe-theme')
					if( heightMenuLang <= 40){
						console.log("NO OCULTAR!!!!");
						$('nav').fadeIn('fast');
					}else{
						console.log("OCULTAR!!!!");
						$('nav').fadeOut('fast');
					}
				}
			}

			showMenuDesktop();

			$('#box-init').css('margin-top', $( '#cover' ).height() );
			function positionMenu(){
				if( $( window ).height() < $(window).scrollTop() ){			
					reposition();				
				}else{
					$('#box-init').css('margin-top', $( '#cover' ).height() - $( window ).scrollTop() );					
				}
			}

			positionMenu();
			reposition();

			function reposition(){
				if ( $(window).scrollTop() > $( window ).height() ){
					$('#box-init').css('margin-top', 0);
				} 
			}


			function positionInFooterMain(){	
				var spaceToFooter = $('#contact').offset().top - $('#contact').outerHeight();
				if ( $(window).scrollTop() > spaceToFooter){
					var topInitBox = $('#contact').outerHeight() - ( $('#contact').offset().top - $(window).scrollTop() );				
					$('#box-init').css('margin-top', - topInitBox);
				}
			}

			$(window).scroll(function() {
				positionInFooterMain();
			});
			positionInFooterMain();
			// positionMenu();
		

			$("#list-menu").on('click', function(event) {
				$("#mobile-main").fadeIn(400);
				$("#mobile-main").removeClass('d-none');
			});


			$("#box-close-menu").click(function(event) {
				$("#mobile-main").fadeOut('400', function() {
					$("#mobile-main").addClass('d-none');	
				});
			});
		
			

		});

		function loadOK(){
				setTimeout(function(){
					$("#box-preload").fadeOut('400', function() {
						
					});
				},500);
			}


		// SWIPE GALERY
		function openPhotoSwipe(item){

			imgs = item.split(',')

			var items = [];
			imgs.forEach(function(value,index){
				items.push({
					src: value,
	            w: 580,
	            h: 768
				});
			});
			console.log(imgs);

	    var pswpElement = document.querySelectorAll('.pswp')[0];

	    // build items array
	    // var items = [
	    //     {
	    //         src: 'https://via.placeholder.com/580x768',
	    //         w: 580,
	    //         h: 768
	    //     },
	    //     {
	    //         src: 'https://via.placeholder.com/580x768',
	    //         w: 580,
	    //         h: 768
	    //     }
	    // ];
	    
	    // define options (if needed)
	    var options = {
				 // history & focus options are disabled on CodePen        
	      	history: false,
	      	focus: false,

	        showAnimationDuration: 0,
	        hideAnimationDuration: 0
	        
	    };
	    
	    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
	    gallery.init();
	};
	</script>

</body>
</html>

<?php wp_footer(); ?>

</body>
</html>
