<?php
/* 

Template Name: Contacto 
*/ 
get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu-fixed' ); ?>
<?php get_template_part( 'template-parts/content', 'contact' ); ?>

<?php
get_footer();