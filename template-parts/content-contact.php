
<?php
	
	$text_page = array();
	
	if ( strpos(get_locale(),'es') !== false ){
		$text_page = array(
			'contact' => 'CONTACTO'
		);	
	}else{
		$text_page = array(
			'contact' => 'CONTACT'
		);
	}

?>

<div id="cover" class="section container-fluid no-gutters">
	<div class="row h-full bg-cover-pepe-theme pt-5">			
		<div class="col-12 align-self-start align-self-lg-center text-center  text-center mt-4">
			
			<img class="text-logo d-none d-lg-block m-auto" src="<?php echo get_template_directory_uri()?>/img/pepe-title.svg">

			<div class="col-12 text-center my-4 d-block d-lg-none">
				<h1 class="face-cr"><?php echo $text_page['contact']?></h1>
			</div>

			<div class="col-12 mt-5 box-social-icons" data-aos="fade-up" data-aos-duration="1000">
				

				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-md-1 my-3 my-lg-4">
							<a href="#">
								<img class="p-2 icon-social-pepe" src="<?php echo get_template_directory_uri()?>/img/icon-tw.svg">
							</a>
						</div>
						<div class="col-12 col-md-1 my-3 my-lg-4">
							<a href="#">
								<img class="p-2 icon-social-pepe" src="<?php echo get_template_directory_uri()?>/img/icon-mail.svg">
							</a>
						</div>
						<div class="col-12 col-md-1 my-3 my-lg-4">
							<a href="#">
								<img class="p-2 icon-social-pepe" src="<?php echo get_template_directory_uri()?>/img/icon-mail.svg">
							</a>
						</div>
						<div class="col-12 col-md-1 my-3 my-lg-4">
							<a href="https://www.instagram.com/pepedavalosoficial/">
								<img class="p-2 icon-social-pepe" src="<?php echo get_template_directory_uri()?>/img/icon-ig.svg">
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>