<?php
/* 

Template Name: Catalogo 
*/ 
get_header();
?>

<?php get_template_part( 'template-parts/content', 'menu' ); ?>
<?php get_template_part( 'template-parts/content', 'catalog' ); ?>

<?php
get_footer();